P(OOP) - Pointless Object Orientated PHP
========================================

Introduction
------------
So what is POOP. POOP is a thought experiment in making PHP act in a more consistent and object orientated way.
Inspired by Languages like Scala and Ruby. POOP intends to make PHP act as if it was a more modern language rather than 
the aging old dog that it is. 

At the base of POOP is a concept called primitive boxing , in which we take the standard PHP primitives such as string,
array , int  and float and wrap them in objects (boxing), we then define a consistent set of methods in order to 
interact with the primitive been boxed.

It will also create objects to group together functions such as file handling. 

### That's (Cool | Stupid) Why?
As I said its a thought experiment to explore the concepts around this idea. 

Its main goal is to make PHP more consistent , as well as adding a more object orientated way of doing things. 
Another affect of this methodology in PHP will be better type hinting , because PHP doesn't support type hinting of 
primitives only objects , when every primitive is an object we can then use the inbuilt type hinting. 

**POOP IS NOT FOR PRODUCTION USE**

One of the things POOPS sets out to explore is the performance hit on using this methodology , the actual performance hit
is unknown at this stage, but because we are creating objects there will be a hit. 