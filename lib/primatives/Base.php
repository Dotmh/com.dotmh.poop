<?php
/**
 * Created by PhpStorm.
 * User: martin.haynes
 * Date: 04/12/2014
 * Time: 12:47
 */

namespace DotMH\POOP\primatives;

abstract class Base {

    private $primative = null;
    private $primativeType = null;

    public function is_a($compare) {

    }

    public function when_unboxed_is() {
        return $this->primativeType;
    }

    public function classname() {
        return get_called_class();
    }

    public function unbox() {
        return $this->primative;
    }

    public static function box($primative) {
        $c = get_called_class();
        return new $c($primative);
    }

}